<?php

namespace models;

class User{
    protected static $tableName= 'Users';

   public static function addUser($login, $password)
    {
        \core\Core::get_Instance()->db->insert(
            self::$tableName, [
                'login'             => $login,
                'password'          => self::hashPassword($password)
            ]
        );
    }
   public static function hashPassword($password){
        return md5($password);
     }
    public static function getUserByLoginAndPassword($login,$password){
        $user= \core\Core::get_Instance()->db->select(self::$tableName,'*',[
            'login'=>  $login,
            'password'=>self::hashPassword($password)
        ]);   

        return !empty($user) ?  $user[0]: NULL;    
        }

    public static function isEmailExists(  $email){
        $user= \core\Core::get_Instance()->db->select(self::$tableName,'*',[
             'login'=>  $email
         ]);      
            return !empty(  $user);
     }
   public static function authenticateUser($user){
      $_SESSION["user"]=$user;
    }
   public static function logoutUser(){
    unset($_SESSION["user"]);
    }
   public static function isUserAuthenticated(){
     return isset($_SESSION['user']);
    }

   public static function getCurrentAuthenticatedUser(){
   if (isset($_SESSION['user'])) {
    return $_SESSION['user'];
   }
    }
    public static function isAdmin(){
        if (isset($_SESSION['user'])) {
            $user =self::getCurrentAuthenticatedUser();
            return $user['access_level']==10;
        }
        }




}