﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodLibrary
{
    public class DomesticSubscription : Subscription
    {
        public DomesticSubscription(List<string> IncludedChannels) : base(9.99m, 1, IncludedChannels) { }
    }

}
