<?php

namespace models;

class UserAchievement
{
    protected static $tableName = 'UsersAchivments';
    public static function addUserAchievement($achiv_id)
    {
         $user = User::getCurrentAuthenticatedUser();
         \core\Core::get_Instance()->db->insert(
            self::$tableName, [
                'user_id' => $user['id'],
                'achiv_id'=> $achiv_id
            ]
        );
    }
     
    public static function getAchievementByUserId()
    {
        $user = User::getCurrentAuthenticatedUser();
        $rows = \core\Core::get_Instance()->db->select(self::$tableName, "*", ['user_id' => $user['id']]);
        return  $rows;
    }
    public static function getAchievementByUserIdAndAchiv_id($achiv_id)
    {
        $user = User::getCurrentAuthenticatedUser();
        $rows = \core\Core::get_Instance()->db->select(self::$tableName, "*", ['user_id' => $user['id'],'achiv_id' => $achiv_id]);
        return  $rows;
    }
    public static function setAchievement()
    {
        $user = User::getCurrentAuthenticatedUser();
        $MatchHistory = MatchHistory::getMatchHistories();
        $achievements = Achievement::getAchievements();
        $count = 0;
        
        foreach ($MatchHistory as $Match) {
            if ($Match['vsBot'] && strpos($Match['winner'], 'player Won') !== false) {
                $count++;
            }
        }
        
        $achivReturn = self::processAchievements($achievements);
    
        foreach ($achievements as $achievement) {
            $arr = UserAchievement::getAchievementByUserIdAndAchiv_id($achievement['id']);
            if (isset($achivReturn[$achievement['id']]) && $count > $achivReturn[$achievement['id']] && empty($arr[0]) &&( empty($arr[0]['achiv_id'])||$arr[0]['achiv_id'] != $achievement['id'])) {
                if (strpos($achievement['achievementsText'], 'Виграти проти бота') === 0) {
                    UserAchievement::addUserAchievement($achievement['id']);
                }
            }

        }
    }
    
    protected static function processAchievements($achievements)
    {
        $achivReturn = [];
        foreach ($achievements as $achievement) {
            preg_match_all('!\d+!', $achievement['achievementsText'], $matches);
            $number = $matches[0][0];
            $achivReturn[$achievement['id']] = $number;
        }
        return $achivReturn;
    }
    
    


}

