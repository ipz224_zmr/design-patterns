﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarySOLID
{
    public class Reporting : IReporting
    {
        private readonly List<WarehouseItem> items;

        public Reporting(List<WarehouseItem> items)
        {
            this.items = items;
        }

        public void RegisterIncome(WarehouseItem item)
        {
           
            items.Add(item);
            Console.WriteLine($"Income registered: {item.Quantity} {item.Unit} of {item.Name}");
        }

        public void RegisterOutcome(WarehouseItem item)
        {
      
            if (items.Contains(item) && item.Quantity <= items.First(i => i.Equals(item)).Quantity)
            {
                items.Remove(item);
                Console.WriteLine($"Outcome registered: {item.Quantity} {item.Unit} of {item.Name}");
            }
            else
            {
                Console.WriteLine($"Error: Insufficient quantity of {item.Name} in the warehouse.");
            }
        }

        public void GenerateInventoryReport()
        {
            Console.WriteLine("Inventory Report:");
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("| Index            | Name            | Unit of Measure | Price | Quantity | Last Restock Date |");
            Console.WriteLine("--------------------------------------------------");
            int i = 1;
            foreach (var item in items)
            {
                Console.WriteLine($"| {i++,-15} | {item.Name,-15} | {item.Unit,-15} | {Money.DisplayAmount(item.Price),-5:C} | {item.Quantity,-8} | {item.LastRestockDate,-19} |");
            }

            Console.WriteLine("--------------------------------------------------");
        }
    }

}
