﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsClassLibrary
{
    public class ThirdLevel : Handler
    {
        protected override string Report => "Переконайтеся в тому чи не зажимає чехол кнопку звуку(якщо наявний),\n вимкніть енергозберігаючий режим,\n проскануйте телефон на наявність вірусів";

        protected override int HelpLevel => 3;

        protected override string HandableProblem => "Пристрій автоматично змінює громкість";

        public override void Inquiry(UserInquiry inquiry)
        {
            if (inquiry.UserProblem.Equals(this.HandableProblem, StringComparison.CurrentCultureIgnoreCase))
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"Рівень {this.HelpLevel} підтримка: {this.Report}");
                Console.ResetColor();
                return;
            }

            base.Inquiry(inquiry);
        }
    }
}
