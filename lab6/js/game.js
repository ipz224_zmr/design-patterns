class TicTacToe {
    constructor(type, row = 3, col = 3) {
        this.row = row;
        this.col = col;
        this.board = Array(this.row * this.col).fill('');
        this.winningCombinations = this.generateWinningCombinations(this.row, this.col, this.row);
        this.currentPlayer = 'X';
        this.gameOver = false;
        this.type = type;
        this.movesHistory = [];
        this.movesHistorySymbol = [];
        this.countMove = null;
        this.comboWin = 0;
        this.doubleMoveCount = 0;
        this.doubleMove = false;
        this.availablePositions = this.checkAvailablePositions();
        this.observer = new Observer();
        this.observer.subscribe(Message.message.bind(this));
    }


    generateWinningCombinations(row, col, winLength) {
        let winningCombinations = [];
        for (let i = 0; i < row; i++) {
            for (let j = 0; j <= col - winLength; j++) {
                let combination = [];
                for (let k = 0; k < winLength; k++) {
                    combination.push(i * col + j + k);
                }
                winningCombinations.push(combination);
            }
        }
        for (let i = 0; i <= row - winLength; i++) {
            for (let j = 0; j < col; j++) {
                let combination = [];
                for (let k = 0; k < winLength; k++) {
                    combination.push((i + k) * col + j);
                }
                winningCombinations.push(combination);
            }
        }
        for (let i = 0; i <= row - winLength; i++) {
            for (let j = 0; j <= col - winLength; j++) {
                let combination = [];
                for (let k = 0; k < winLength; k++) {
                    combination.push((i + k) * col + (j + k));
                }
                winningCombinations.push(combination);
            }
        }
        for (let i = 0; i <= row - winLength; i++) {
            for (let j = col - 1; j >= winLength - 1; j--) {
                let combination = [];
                for (let k = 0; k < winLength; k++) {
                    combination.push((i + k) * col + (j - k));
                }
                winningCombinations.push(combination);
            }
        }

        return winningCombinations;
    }
    checkWinner() {
        for (let combo of this.winningCombinations) {
            let isWinningCombo = true;
            let player = this.board[combo[0]];
            if (player === '') continue;
            for (let index of combo) {
                if (this.board[index] !== player) {
                    isWinningCombo = false;
                    break;
                }
            }
            if (isWinningCombo) {
                this.gameOver = true;
                this.comboWin = 1;
                return true;
            }
        }

        if (!this.board.includes('')) {
            this.gameOver = true;
            return true;
        }
        return false;
    }



    checkAvailablePositions() {
        let availablePositions = [];
        for (let i = 0; i < this.board.length; i++) {
            if (this.board[i] === '') {
                availablePositions.push(i);
            }
        }
        return availablePositions;
    }

    winOrTie(Winner, winnerName = "player") {
        let result = "";
        if (!this.board.includes('')) {
            if (this.comboWin != 1) {
                result = 'It\'s a tie!';
            } else {
                result = `${winnerName} Won ${Winner}`;
            }
        } else {
            result = `${winnerName} Won ${Winner}`;
        }
        let lostPlayer;
        if (this.type == '1vs1') {
            lostPlayer = `${this.currentPlayer === 'X' ? 'O' : 'X'}`;
        } else if (this.type == "vsBot") {
            if (winnerName == "player") {
                lostPlayer = `${this.currentPlayer === 'X' ? 'O' : 'X'}`;
            } else if (winnerName == "bot") {
                lostPlayer = `${this.currentPlayer === 'X' ? 'X' : 'O'}`;
            }
        }
        if (result == "It\'s a tie!") {
            lostPlayer = "It\'s a tie!";
        }


        this.observer.notify(result);
        let type = this.type == "vsBot" ? 1 : 0;
        const urlParams = new URLSearchParams(window.location.search);
        let doubleMove = urlParams.get('doubleMove') === "true" ? 1 : 0;

        CookiesAction.handleWin(this.row, this.col, this.movesHistory, result, lostPlayer, type, doubleMove, this.movesHistorySymbol);
        Helper.disableButtons();
        document.getElementById('player-won').parentElement.classList.remove('hide');
    }

    handleMove(position, doubleMove = false) {
        if (this.type=="1vs1"&&this.doubleMove==true) {
            this.doubleMove = true;
        }else{
            this.doubleMove = doubleMove;
        }
      
       
        if (!this.gameOver && this.board[position] === '') {
            this.updateBoardAfterMove(position);
            if (this.doubleMove && this.row > 3&&this.type!="vsBot") {
                this.doubleMoveCount++;
            }

            if (this.doubleMoveCount === 2) {
                this.currentPlayer = this.currentPlayer === 'X' ? 'X' : 'O';
                document.getElementById('player-turn').innerText = this.currentPlayer;
                this.doubleMoveCount = 0;
            }
            this.switchPlayerOrBotMove(this.doubleMoveCount);
        }
    }
    updateBoardAfterMove(position) {
        this.board[position] = this.currentPlayer;
        this.movesHistory.push(position);
        this.movesHistorySymbol.push(this.currentPlayer);
        document.getElementById(`btn-${position}`).innerText = this.currentPlayer;
    }


    switchPlayerOrBotMove(doubleMoveCount) {
        if (this.checkWinner()) {
            this.winOrTie(this.currentPlayer);
        } else {
            if (doubleMoveCount === 0) {
                if (this.type == "1vs1") {
                    this.currentPlayer = this.currentPlayer === 'X' ? 'O' : 'X';
                    document.getElementById('player-turn').innerText = this.currentPlayer;
                    this.countMove = 0;
                } else if (this.type === "vsBot") {
                    this.botMove();
                    this.countMove = 0;
                }
            }
        }
    }



    undoblock() {
        let lastMove = this.movesHistory.pop();
        this.movesHistorySymbol.pop();
        this.board[lastMove] = '';
        document.getElementById(`btn-${lastMove}`).innerText = '+';
    }

    undoMove() {
        if (this.movesHistory.length > 0) {
            if (this.countMove == 0) {
                if (this.gameOver == false) {
                    this.undoblock();
                    if (this.type == "vsBot") {
                        this.undoblock();
                        this.currentPlayer = this.currentPlayer === 'X' ? 'O' : 'X';
                    }
                    this.currentPlayer = this.currentPlayer === 'X' ? 'O' : 'X';
                }

                this.countMove = 1;


            }
        }
    }

    saveGame() {
        let type = this.type == "vsBot" ? 1 : 0;
        const urlParams = new URLSearchParams(window.location.search);
        let doubleMove = urlParams.get('doubleMove') === "true" ? 1 : 0;
        CookiesAction.SaveInformation(this.row, this.col, this.movesHistory, this.currentPlayer, type, doubleMove, this.movesHistorySymbol);
    }

    returnGame(tableGenerator) {
        let cookie = CookiesAction.getCookies();
        this.movesHistory = cookie['sequence'].split(',');
        this.movesHistorySymbol = cookie['symbols'].split(',');
        this.row = parseInt(cookie['row']);
        this.col = parseInt(cookie['col']);
        tableGenerator.generateTable(this.row, this.col);
        this.winningCombinations = this.generateWinningCombinations(this.row, this.col, this.row);
        this.availablePositions = this.checkAvailablePositions();
        this.type = cookie['vsBot'] == 0 ? "vsBot" : "1vs1";
        this.currentPlayer = cookie['userSide'];
        this.doubleMove =parseInt(cookie['typeGame'])==1 ? true:false;
        this.board = Array(this.row * this.col).fill('');
        tableGenerator.sequence = this.movesHistory.join(',');
        tableGenerator.symbols = this.movesHistorySymbol.join(',');
        tableGenerator.moves = new Array(this.row * this.col).fill('+');

        for (let i = 0; i < this.row * this.col; i++) {
            if (tableGenerator.sequence.includes(i.toString())) {
                let index = tableGenerator.sequence.indexOf(i.toString());
                tableGenerator.moves[i] = tableGenerator.symbols[index];
            }
        }


        tableGenerator.updateTable();

        for (let i = 0; i < this.row * this.col; i++) {
            if (tableGenerator.moves[i] !== '+') {
                let button = document.getElementById(`btn-${i}`);
                button.textContent = tableGenerator.moves[i];
                this.board[i] = tableGenerator.moves[i];
            }
        }
        let lastMoveSymbol = this.movesHistorySymbol[this.movesHistorySymbol.length - 1];
        this.currentPlayer = lastMoveSymbol === 'X' ? 'O' : 'X';

       
    }




    restartGame() {
        location.reload();
    }

    giveUp() {
        const winner = this.currentPlayer === 'X' ? 'O' : 'X';
        this.gameOver = true;
        Helper.disableButtons();
        Message.message(`Player Won ${winner}`);
        document.getElementById('player-won').parentElement.classList.remove('hide');
    }
}