﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyClassLibrary
{
    public class SmartTextChecker : ITextReader
    {
        private SmartTextReader _reader;

        public SmartTextChecker()
        {
            _reader = new SmartTextReader();
        }

        public char[][] ReadText(string filePath)
        {
            Console.WriteLine("Opening file: " + filePath);
            char[][] result = _reader.ReadText(filePath);
            if (result != null)
            {
                Console.WriteLine("File successfully read.");
                Console.WriteLine("Total lines: " + result.Length);
                int totalChars = 0;
                foreach (var line in result)
                {
                    totalChars += line.Length;
                }
                Console.WriteLine("Total characters: " + totalChars);
            }
            Console.WriteLine("Closing file: " + filePath);
            return result;
        }
    }
}
