class TableGenerator {
    constructor(containerId, sequence = null, symbols = null) {
        this.containerId = containerId;
        this.container = document.getElementById(containerId);
        this.currentIndex = 0;
        this.moves = null;
        this.row = null;
        this.col = null;
        this.sequence = sequence;
        this.symbols = symbols;
    }

    generateTable(row = 3, col = 3) {
        this.row = row;
        this.col = col;
        this.moves = new Array(row * col).fill('+');
        let table = document.createElement('table');
        for (let i = 0; i < row; i++) {
            let row = table.insertRow();

            for (let j = 0; j < col; j++) {
                let cell = row.insertCell();
                let button = document.createElement('button');
                let position = i * col + j;
                button.textContent = `+`;
                button.classList.add('styletable');
                button.id = `btn-${position}`;
                cell.appendChild(button);
            }
        }

        this.container.innerHTML = '';

        this.container.appendChild(table);
        this.updateTable();
    }
    updateTable() {

        for (let i = 0; i < this.row; i++) {
            for (let j = 0; j < this.col; j++) {
                let position = i * this.col + j;
                let button = document.getElementById(`btn-${position}`);
                button.textContent = this.moves[position];
            }
        }
    }


    moveForward() {
        if (this.currentIndex < this.sequence.split(',').length) {
            let currentMove = this.sequence.split(',').map(Number)[this.currentIndex];
            let symbol = this.symbols.split(',').map(String)[this.currentIndex];
            this.moves[currentMove] = symbol;
            this.currentIndex++;
            this.updateTable();
        }
    }




    moveBackward() {
        if (this.currentIndex > 0) {
            this.currentIndex--;
            let currentMove = this.sequence.split(',').map(Number)[this.currentIndex];
            this.moves[currentMove] = '+';
            this.updateTable();
        }
    }

}
