<?
namespace core;

class QueryBuilder {
    protected $query;
    protected $params = [];
    
    public function select($fields = "*") {
        $this->query = "SELECT $fields";
        return $this;
    }
    
    public function from($table) {
        $this->query .= " FROM $table";
        return $this;
    }
    
    public function where($conditions = []) {
        if (!empty($conditions)) {
            $where = implode(' AND ', array_map(function($key) {
                return "$key = :$key";
            }, array_keys($conditions)));
            $this->query .= " WHERE $where";
            $this->params = $conditions;
        }
        return $this;
    }
    
    public function getQuery() {
        return $this->query;
    }
    
    public function getParams() {
        return $this->params;
    }
}