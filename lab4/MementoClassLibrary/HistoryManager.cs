﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MementoClassLibrary
{
    public class HistoryManager
    {
        private Stack<TextDocument> history;

        public HistoryManager()
        {
            history = new Stack<TextDocument>();
        }

        public void Save(TextDocument document)
        {
            TextDocument snapshot = new TextDocument(document.Content);
            history.Push(snapshot);
        }

        public TextDocument Undo()
        {
            if (history.Count > 0)
            {
                return history.Pop();
            }
            return null;
        }
    }
}
