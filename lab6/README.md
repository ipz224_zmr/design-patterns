# Хрестики-Нулики


## Запуск програми

Для початку потрібно мати openserver  з відповідними версіями:
- **Apache_2.4-PHP_8.0-8.1**
- **PHP_8.1**
- **MySQL-5.7-Win10**

В PhpMyAdmin потрібно зайти під root з паролем "" та використати import файлу  [XO_game.sql](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/backup/XO_game.sql?ref_type=heads)

## Функціонал  програми

Після імпорту потрібно зареєструватися або виконати вхід під логином: **admin@gmail.com** з паролем **admin** , для того , щоб зберігалися матчі , були наявні досягнення, історія усіх матчів, та послідовний перегляд. 

Звичайно можна пограти в гру без реєстрації.

На головному екрані  ми можемо бачити  посилання на гру **1х1** , гру **проти бота**, якщо зареєстрований , то також посилання на **досягнення** та **історію перемог**.
### Історія матчів
У **Історії матчів** ми можемо переглянути результати минулих матчів, також там наявне посилання на цей матч, щоб послідовно переглянути його.
### Досягнення
У  **Досягненнях** можна побачити наявні досягнення у гравця, якщо зайти під   **admin@gmail.com**  то можна буде видаляти , додавати , редагувати та переглядати досягнення. 
### 1x1
У грі **1x1** можна обирати:
- Розмір таблиця від 3х3 до 5х5
- Перезапуск гри
- Кнопку здатися
- Зміну режиму на гру проти бота
- Зміну режиму на 1х1
- Зберегти гру
- Відновити гру
- Крок назад
- Також при грі 4х4 або 5х5 доступний режим подвійного кроку


Режим **1x1** відбувається завдяки послідовними кроками від двох гравців на одному пк. 

На більших дошка діють такі ж правила ,як і у стандартній грі.

### проти бота
У грі **проти бота** можна обирати:
- Розмір таблиця від 3х3 до 5х5
- Обрати сторону **x** або **o**
- 3 рівня складності
- Перезапуск гри
- Кнопку здатися
- Зміну режиму на гру проти бота
- Зміну режиму на 1х1
- Крок назад

У залежності від складності гри бот буде мати можливість:

- Рандомно рухатися по дошці
- Збирати переможні комбінації
- Заважати гравцю  перемогти (руйнувати переможні комбінації)

## Design Patterns

### Singleton

Використовується для створення лише одного об'єкту ядра програми: [Core](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/core/Core.php?ref_type=heads#L16)

### Observer

Використовується для сповіщення користувача про зміни в грі (перемогу , поразку або нічию) [Observer](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/js/game.js?ref_type=heads#L126)


### Command

У коді є виклики методів, які виконують певні дії, наприклад, [game.undoMove()](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/js/main.js?ref_type=heads#L34), game.saveGame(), game.returnGame(). Це відповідає принципу команди, де запити на виконання дій здійснюються через об'єкти-команди.

### Factory Method

[TicTacToeFactory](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/js/ticTacToeFactory.js?ref_type=heads) - Це фабричний клас, який має статичний метод createGame. Цей метод приймає тип гри (type) і деякі додаткові параметри, такі як розмір поля, сторона гравця та рівень складності.
На основі переданого типу гри він створює відповідний об'єкт гри.
Залежно від типу гри, він створює об'єкт класу OneVsOneTicTacToe або VsBotTicTacToe.


### Front Controller

У коді використовується шаблон [Front Controller](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/core/Core.php?ref_type=heads#L32) в класі Core. Цей клас відповідає за маршрутизацію запитів, вибір відповідного контролера та дії для обробки запиту, а також відображення сторінок, враховуючи результати обробки.


###  Strategy

Паттерн "стратегія" виявляється в тому, як у коді обирається стратегію для ходу бота в залежності від рівня складності гри. 
Визначаєте рівень складності (easy, middif, highdif) у методі [checkDificultGame()](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/js/vsBotTicTacToe.js?ref_type=heads#L30), а потім використовуєте цей рівень, щоб визначити, який хід бота буде виконаний в методі botMove().
Зазвичай у паттерні "стратегія" створюються різні стратегії (класи),які реалізують один і той же інтерфейс, але у моєму випадку, стратегії (рівні складності) визначаються в межах одного класу VsBotTicTacToe, але ідея зберігається - у коді обирається одна з декількох можливих стратегій в залежності від поточного контексту (складності гри).


###  MVC

(Model-View-Controller): Це архітектурний шаблон, де логіка додатку поділена на три основні частини: модель (Model), представлення (View) і контролер (Controller). У цьому коді [controllers](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/tree/main/controllers?ref_type=heads), [models](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/tree/main/models?ref_type=heads) і [views](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/tree/main/views?ref_type=heads) розділені на відповідні частини.

### Active  Record

Класи Achievement, MatchHistory, SaveGame, та User можна розглядати як представники активного запису. Кожен з цих класів відображає таблицю в базі даних та має методи для роботи з записами цих таблиць (додавання, видалення, редагування тощо). Наприклад, [MatchHistory::addMatchHistory($data)](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/models/MatchHistory.php?ref_type=heads#L8) додає запис у таблицю MatchHistory.


### Service Locator
 У класах Achievement, MatchHistory, SaveGame, та User використовується [core\Core::get_Instance()->db](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/models/Achievement.php?ref_type=heads#L10), щоб отримати доступ до об'єкту бази даних. Це можна розглядати як патерн "локатор служб", де клас Core використовується для локалізації потрібних служб, таких як з'єднання з базою даних.

 
### Builder

[Builder](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/core/QueryBuilder.php?ref_type=heads) використовується для побудови select запитів: у класі [DB](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/core/DB.php?ref_type=heads#L15). 


## Refactoring Techniques


### Move Method

Переміщення методів у інший клас з метою дотримання  принципів програмування  переміщено з [Message](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/commit/14d539e1372857ef814b4061ea983d57b2cbdd3b#d64c980a7906dcd5fa3a3c90c79cb630fe43df66) до [Cookies](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/commit/14d539e1372857ef814b4061ea983d57b2cbdd3b#e2723da188c8007513295e4ec44cc5a8e5395692)

### Add Parameter

Додавання параметру для покращення роботи програми приклад: 
- [ gameActive()](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/commit/129db59c47530241b1e84063e6666890800d9eb7#dae13fd6f331d1c91ab58b06e81d05cb8958bfb1) 
- [generateWinningCombinations()](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/commit/22b6f75bce353604bd2917e786b369c2b29645a6#a11dffb636ca569515545ae88e1b79af1707c694_18_17)


### Preserve Whole Object

Передача даних не по одному, а в масиві  [addMatchHistory()](
https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/commit/dc083f763e9b022e240fc8c9d416e1298e18993e#db6e2051f4092485853a574110fd4dfb127e5428_9_8)

### Rename Method

На етапі розробки цей метод постійно використовувався при зміні роботи методу.

### Extract Variable
Спрощення складного виразу , перевірка чи кукі != null:
[parent::areCookiesSet](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/commit/f39f2adc4503f873d1d70d3c93df0ec2e20f0047#c343e6a3bbd7ad7feadb6fbc53c5fb1c3ec456e5_33_29)

Або

Відокремлено валідацію полів у інший статичний клас [Validate](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/models/Validate.php?ref_type=heads)   його використання: 
  [Validate::validateRegisterForm($_POST)](
https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/commit/a7a50d0665e555ceddcea1954d1283e48359996c#97a40b1f32dc75644565404e19c00cf7917d3d5f_0_27)


###  Extract Method

На етапі розробки часто використовувався з метою уникнення дублювань коду.


### Inline Method

На етапі розробки  використовувався з метою   спрощення розуміння коду.

### Inline Temp

На етапі розробки  використовувався для зменшення розміру коду.


### Move Field

На етапі розробки  використовувався для спрощення коду , покращення логіки та відповідності поля до класу.

### Substitute Algorithm

Заміна перевірки кожного по одинці в [GameController](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/commit/f39f2adc4503f873d1d70d3c93df0ec2e20f0047#c343e6a3bbd7ad7feadb6fbc53c5fb1c3ec456e5_33_29) на перевірку алгоритмом:
[areCookiesSet](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/core/Cookies.php?ref_type=heads#L22)


### Extract Class

Використовується завдяки зв'язку many-to-many між  [User](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/models/User.php?ref_type=heads) та [Achievement](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/models/Achievement.php?ref_type=heads) , а саме в моделі [UserAchievement](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/models/UserAchievement.php?ref_type=heads) завдяки методу [getAchievementByUserId()](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/models/UserAchievement.php?ref_type=heads#L19) 


## Programming Principles

### DRY


Для уникнення дублювання коду зроблена спеціальна система, щоб   не створювати кожен раз нове view ми просто заповнюємо іншими даними його, у такому випадку у нас виходить посилання /game/view/[$id](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/controllers/GameController.php?ref_type=heads#L85), тобто будь-яке число.

###  (Open/Closed Principle)
Код легко розширюється для внесення змін, але залишається стійким до модифікацій. Наприклад, нові контролери можна додати без змін в коді Core.


### YAGNI 
Код уникає непотрібної складності та функцій, які не потрібні негайно. Він реалізує лише функції, необхідні для керування.

### Liskov Substitution Principle
Об'єкти можуть бути замінені їх підтипами без зміни функціональності. Наприклад, усі класи контролерів повинні наслідувати клас  [Controller](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/core/Controller.php?ref_type=heads).

### Fail Fast

Використовується перевірка при [реєстрації](https://gitlab.com/2022-2026/ipz-22-4/zubyk-maksym/design-patterns-lab6/-/blob/main/controllers/UserController.php?ref_type=heads#L27) або входу в акаунт. 

###  Big Design Up Front

Використовувався при проектуванні ядра, зв'язку з базою даних , роботи контролерів та роботи з моделю тобто побудови роботи MVC проекта.