document.addEventListener("DOMContentLoaded", function () {
    let matchesHistory = JSON.parse(document.querySelector('#MatchesHistory').value);
    let tableGenerator = new TableGenerator('gameTable', matchesHistory[0]["sequence"],matchesHistory[0]["symbols"]);


    tableGenerator.generateTable(matchesHistory[0]["row"], matchesHistory[0]["col"]);

    document.querySelector('.btn-forward').addEventListener('click', function () {
        tableGenerator.moveForward();
    });

    document.querySelector('.btn-backward').addEventListener('click', function () {
        tableGenerator.moveBackward();
    });
});
