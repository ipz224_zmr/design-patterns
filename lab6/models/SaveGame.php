<?php

namespace models;

class SaveGame {
    protected static $tableName= 'SaveGame';

    public static function addSaveGame($data)
    {
        $user = User::getCurrentAuthenticatedUser();
  
            self::deleteSaveGame();
   
        \core\Core::get_Instance()->db->insert(
            self::$tableName, [
                'sequence'     => $data['sequence'],
                'userSide'       => $data['userSide'],
                'row'          => $data['row'],
                'col'          => $data['col'],
                'vsBot'        => $data['vsBot'],
                'typeGame'   => $data['typeGame'],
                "user_id"      => isset($user['id'])? $user['id']: null,
                "symbols"      => $data['symbols']
            ]
        );
    }
    public static function deleteSaveGame()
    {
        $user = User::getCurrentAuthenticatedUser();
        $saveGame = self::getSaveGameByUserId();
    
        if ($saveGame) {
            \core\Core::get_Instance()->db->delete(
                self::$tableName, [
                    'user_id' => $user['id']
                ]
            );
        }
    }
    public static function getSaveGameByUserId()
    {
        $user = User::getCurrentAuthenticatedUser();
       $rows= \core\Core::get_Instance()->db->select(
            self::$tableName,'*',[
                'user_id'       => $user['id']           
            ]
        );
        return $rows[0]??null;
       
    }
}