<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script  src="/js/table.js"></script>
</head>
<body>
<h1>Історія Ходів</h1>

<div class="row" style="max-height:800px;">
<div class="col-6">
<div id="gameTable" class="p-3 m-2"></div>
</div>
<div class="col-6">
<div >
    <button class="btn btn-success m-1 btn-backward">Крок назад</button>
    <button class="btn btn-success m-1 btn-forward">Крок вперед</button>
</div>
</div>
</div>
<input type="hidden" id="MatchesHistory" value="<?php echo htmlspecialchars(json_encode($MatchesHistory)); ?>">
<script  src="/js/victory.js"></script>
</body>
</html>
