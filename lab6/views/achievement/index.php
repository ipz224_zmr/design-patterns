<?
use models\User;
?>




<?if(User::isAdmin()):?>
<div class="m-3">
<a href="http://lab6/achievement/add" class="btn btn-primary">Додати досягнення</a>
</div>

<?endif;?>

<div class="container" style="  max-height: 400px;
    overflow-y: auto;">
    <?php foreach($achievements as $achievement):?>
        <?php      
            $userHasAchievement = $UserAchivHaven[$achievement['id']] ?? false;
            $cardClass = $userHasAchievement ? 'text-bg-warning' : 'text-bg-secondary';
        ?>

        <div class="card <?=$cardClass?> mb-3" style="max-width: 100%;">
            <div class="card-body">
                <p class="card-text"><?=$achievement['achievementsText']?></p>
                <?php if(User::isAdmin()):?>
                    <a href="http://lab6/achievement/edit/<?=$achievement['id']?>" class="btn btn-primary">Редагувати</a>
                    <a href="http://lab6/achievement/delete/<?=$achievement['id']?>" class="btn btn-primary">Видалити</a>
                <?php endif;?>
            </div>
        </div>

    <?php endforeach;?>
</div>