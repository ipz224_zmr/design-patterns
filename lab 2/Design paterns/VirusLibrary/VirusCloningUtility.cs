﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirusLibrary
{
    public static class VirusCloningUtility
    {
        public static T PerformCopy<T>(this ICopyableVirus<T> virus)
        where T : new()
        {
            return virus.PerformCopy();
        }
    }
}
