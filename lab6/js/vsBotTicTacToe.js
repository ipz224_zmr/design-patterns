class VsBotTicTacToe extends TicTacToe {
    constructor(type, row, col, side, dificult) {
        super(type, row, col);
        this.row = row;
        this.col = col;
        this.type = type;
        this.currentPlayer = side;
        this.dificult = dificult;
        if (side == 'O') {
            this.botMove();
        }
    }
    randomPosition(bot) {
        const availablePositions = this.checkAvailablePositions();
        const randomPosition = availablePositions[Math.floor(Math.random() * availablePositions.length)];
        if (this.board[randomPosition] === '') {
            this.board[randomPosition] = bot;
            document.getElementById(`btn-${randomPosition}`).innerText = bot;
            this.movesHistory.push(randomPosition);
            this.movesHistorySymbol.push(bot);
        }
    }
    counterAction(botmove, bot) {
        this.board[botmove] = bot;
        document.getElementById(`btn-${botmove}`).innerText = bot;
        this.movesHistory.push(botmove);
        this.movesHistorySymbol.push(bot);
    }

    checkDificultGame() {
        switch (this.dificult) {
            case "easy":
            default:

                return 1;
            case "middif":

                return 2;
            case "highdif":

                return 3;
        }
    }

    botMove() {
        let bot = this.currentPlayer === 'X' ? 'O' : 'X';
        let dif = this.checkDificultGame();
        let botWinningMove = -1;
        let playerWinningMove = -1;
        for (let combination of this.winningCombinations) {
            let botCount = 0;
            let playerCount = 0;
            let emptyIndex = -1;
            for (let index of combination) {
                if (this.board[index] === bot) {
                    botCount++;
                } else if (this.board[index] === '') {
                    emptyIndex = index;
                } else {
                    playerCount++;
                }
            }
            if (botCount === this.row - 1 && playerCount === 0) {
                botWinningMove = emptyIndex;
            }
            if (playerCount === this.row - 1 && botCount === 0) {
                playerWinningMove = emptyIndex;
            }
        }
        if (botWinningMove !== -1 && this.board[botWinningMove] === '' && dif == 3) {

            this.counterAction(botWinningMove, bot);
        } else if (playerWinningMove !== -1 && this.board[playerWinningMove] === '' && dif > 1) {

            this.counterAction(playerWinningMove, bot);
        } else {

            this.randomPosition(bot);
        }
        if (this.checkWinner()) {

            this.winOrTie(bot, 'bot');

        }

    }

}