class CookiesAction {
  
    static handleWin(row, col, moveHistory, Winner, lostPlayer, vsBot,doubleMove, moveHistorySymbol) {
        let moveHistoryString = moveHistory.join(',');
        let moveHistorySymbolString = moveHistorySymbol.join(',');
        document.cookie = `row=${row}`;
        document.cookie = `col=${col}`;
        document.cookie = `moveHistory=${moveHistoryString}`;
        document.cookie = `winner=${Winner}`;
        document.cookie = `lostPlayer=${lostPlayer}`;
        document.cookie = `vsBot=${vsBot}`;
        document.cookie = `doubleMove=${doubleMove}`;
        document.cookie = `moveHistorySymbol=${moveHistorySymbolString}`;
    }
    static SaveInformation(row, col, moveHistory, playerSide, vsBot,doubleMove, moveHistorySymbol) {
        let moveHistoryString = moveHistory.join(',');
        let moveHistorySymbolString = moveHistorySymbol.join(',');
        document.cookie = `row=${row}`;
        document.cookie = `col=${col}`;
        document.cookie = `moveHistory=${moveHistoryString}`;
        document.cookie = `playerSide=${playerSide}`;
        document.cookie = `vsBot=${vsBot}`;
        document.cookie = `typeGame=${doubleMove}`;
        document.cookie = `moveHistorySymbol=${moveHistorySymbolString}`;
    }
    static getCookies(){
        var cookies = document.cookie.split(';');
        var cookieData = {};
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim().split('=');
            var key = cookie[0];
            var value = cookie[1];
            cookieData[key] = decodeURIComponent(value);
        }
        return    cookieData;
    }
}
