-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 29 2024 г., 13:35
-- Версия сервера: 5.7.39
-- Версия PHP: 8.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `XO_game`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Achievements`
--

CREATE TABLE `Achievements` (
  `id` int(11) NOT NULL,
  `achievementsText` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `Achievements`
--

INSERT INTO `Achievements` (`id`, `achievementsText`) VALUES
(1, 'Виграти проти гравця 1 рази'),
(3, 'Виграти проти гравця 4 рази'),
(4, 'Виграти проти гравця 5 разів'),
(9, 'Виграти проти  гравця 10 раз'),
(10, 'Виграти проти гравця 50 раз\r\n'),
(11, 'Виграти проти гравця 100 раз\r\n'),
(12, 'Виграти проти гравця 200 раз\r\n'),
(13, 'Виграти проти гравця 500 раз\r\n'),
(14, 'Виграти проти гравця 1000 раз\r\n'),
(15, 'Виграти проти гравця 10000 раз');

-- --------------------------------------------------------

--
-- Структура таблицы `MatchHistory`
--

CREATE TABLE `MatchHistory` (
  `id` int(11) NOT NULL,
  `sequence` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `winner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `row` int(11) NOT NULL,
  `col` int(11) NOT NULL,
  `lostPlayer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `vsBot` tinyint(1) NOT NULL DEFAULT '0',
  `doubleMove` tinyint(1) NOT NULL DEFAULT '0',
  `symbols` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `MatchHistory`
--

INSERT INTO `MatchHistory` (`id`, `sequence`, `winner`, `row`, `col`, `lostPlayer`, `user_id`, `vsBot`, `doubleMove`, `symbols`) VALUES
(121, '0,1,2,4,3,7', 'player Won O', 3, 3, 'X', 1, 0, 0, 'X,O,X,O,X,O'),
(122, '0,1,6,7,13,12,11,17,16,22,20,15,21,10,5,24,18,23,19,8,4,9,14,2', 'player Won O', 5, 5, 'X', 1, 0, 0, 'X,O,X,O,X,O,X,O,X,O,X,O,X,O,X,O,X,O,X,O,X,O,X,O'),
(125, '0,1,5,6,2,3', 'player Won X', 4, 4, 'O', 1, 0, 1, 'X,X,O,O,X,X'),
(126, '0,4,1,5,2', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(141, '0,3,1,4,2', 'player Won X', 3, 3, 'O', 1, 1, 0, 'X,O,X,O,X'),
(142, '0,4,1,6,2', 'player Won X', 3, 3, 'O', 1, 1, 0, 'X,O,X,O,X'),
(143, '0,2,1,7,4,6,3,5,8', 'player Won X', 3, 3, 'O', 1, 1, 0, 'X,O,X,O,X,O,X,O,X'),
(144, '0,1,5,4,7,6,3,2', 'player Won O', 3, 3, 'X', 1, 1, 0, 'X,O,X,O,X,O,X,O'),
(145, '0,6,4,8,1,7', 'bot Won O', 3, 3, 'X', 1, 1, 0, 'X,O,X,O,X,O'),
(146, '4,3,7,1,0,8,5,6,2', 'It\'s a tie!', 3, 3, 'It\'s a tie!', 1, 1, 0, 'X,O,X,O,X,O,X,O,X'),
(147, '0,8,1,7,2', 'player Won X', 3, 3, 'O', 1, 1, 0, 'X,O,X,O,X'),
(148, '0,7,1,4,2', 'player Won X', 3, 3, 'O', 1, 1, 0, 'X,O,X,O,X'),
(149, '0,6,1,11,2,7,3', 'player Won X', 4, 4, 'O', 1, 1, 0, 'X,O,X,O,X,O,X'),
(150, '0,7,1,13,2,15,3', 'player Won X', 4, 4, 'O', 1, 1, 0, 'X,O,X,O,X,O,X'),
(151, '0,11,1,12,2,6,3', 'player Won X', 4, 4, 'O', 1, 1, 0, 'X,O,X,O,X,O,X'),
(152, '0,20,1,3,2,19,5,15,6,22,12,10,18,16,24', 'player Won X', 5, 5, 'O', 1, 1, 0, 'X,O,X,O,X,O,X,O,X,O,X,O,X,O,X'),
(153, '0,8,6,7,12,11,18,4,24', 'player Won X', 5, 5, 'O', 1, 1, 0, 'X,O,X,O,X,O,X,O,X'),
(154, '0,9,6,15,12,14,18,11,24', 'player Won X', 5, 5, 'O', 1, 1, 0, 'X,O,X,O,X,O,X,O,X'),
(155, '0,8,1,5,2,6,3,10,4', 'player Won X', 5, 5, 'O', 1, 1, 0, 'X,O,X,O,X,O,X,O,X'),
(156, '0,1,7,8,2,3,9,13,11,16,22,23,17,18,6,12,5,10,21,15,20,24,19,14,4', 'player Won X', 5, 5, 'O', 1, 0, 1, 'X,X,O,O,X,X,O,O,X,X,O,O,X,X,O,O,X,X,O,O,X,X,O,O,X'),
(157, '0,1,2,5,4,7,8', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X,O,X'),
(158, '0,3,6,7,4,5,8', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X,O,X'),
(159, '0,5,4,8,1,2', 'bot Won O', 3, 3, 'X', 1, 1, 0, 'X,O,X,O,X,O'),
(160, '0,6,1,4,2', 'player Won X', 3, 3, 'O', 1, 1, 0, 'X,O,X,O,X'),
(168, '0,4,1,7,5,3,6,8,2', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X,O,X,O,X'),
(174, '0,1,5,6,10,15,14,11,13,12,8,2,4,9,7,3', 'player Won O', 4, 4, 'X', 1, 0, 0, 'X,O,X,O,X,O,X,O,X,O,X,O,X,O,X,O'),
(176, '0,1,7,8,12,13,17,16,11,6,18,23,5,10,21,22,15,20', 'player Won X', 5, 5, 'O', 1, 0, 0, 'X,X,O,O,X,X,O,O,X,X,O,O,X,X,O,O,X,X'),
(177, '6,7,12,13,18,17,16,11,8,3,2,1,5,14,10,15,9', 'player Won X', 5, 5, 'O', 1, 0, 1, 'X,X,O,O,X,X,O,O,X,X,O,O,X,X,O,O,X'),
(178, '0,1,6,7,8,13,18,17,12,19,24', 'player Won X', 5, 5, 'O', 1, 0, 0, 'X,O,X,O,X,O,X,O,X,O,X'),
(179, '0,2,4,1,8', 'player Won X', 3, 3, 'O', 1, 1, 0, 'X,O,X,O,X'),
(180, '0,1,4,8,5,2,3', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X,O,X'),
(181, '0,1,4,2,8', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(182, '0,4,1,5,2', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(183, '0,4,1,5,2', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(184, '0,4,1,5,2', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(185, '1,4,5,7,3,0,6,8', 'player Won O', 3, 3, 'X', 1, 0, 0, 'X,O,X,O,X,O,X,O'),
(186, '1,4,5,7,3,0,6,2,8', 'It\'s a tie!', 3, 3, 'It\'s a tie!', 1, 0, 0, 'X,O,X,O,X,O,X,O,X'),
(187, '0,1,4,5,8', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(188, '0,4,5,1,7,6,2,3,8', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X,O,X,O,X'),
(189, '7,5,8,4,6', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(190, '4,2,5,1,3', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(191, '4,5,7,3,1', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X'),
(192, '0,2,4,1,8', 'player Won X', 3, 3, 'O', 2, 1, 0, 'X,O,X,O,X'),
(193, '0,1,4,8,6,3,7,5,2', 'player Won X', 3, 3, 'O', 2, 1, 0, 'X,O,X,O,X,O,X,O,X'),
(194, '0,4,1,5,2', 'player Won X', 3, 3, 'O', 2, 0, 0, 'X,O,X,O,X'),
(195, '0,1,4,5,8', 'player Won X', 3, 3, 'O', 1, 0, 0, 'X,O,X,O,X');

-- --------------------------------------------------------

--
-- Структура таблицы `SaveGame`
--

CREATE TABLE `SaveGame` (
  `id` int(11) NOT NULL,
  `sequence` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbols` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `userSide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vsbot` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `col` int(11) NOT NULL,
  `typeGame` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `SaveGame`
--

INSERT INTO `SaveGame` (`id`, `sequence`, `symbols`, `userSide`, `vsbot`, `user_id`, `row`, `col`, `typeGame`) VALUES
(48, '0,1,4,5', 'X,O,X,O', 'X', 0, 1, 3, 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_level` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `Users`
--

INSERT INTO `Users` (`id`, `login`, `password`, `access_level`) VALUES
(1, 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 10),
(2, 'qwerty@gmail.com', 'd8578edf8458ce06fbc5bb76a58c5ca4', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `UsersAchivments`
--

CREATE TABLE `UsersAchivments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `achiv_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `UsersAchivments`
--

INSERT INTO `UsersAchivments` (`id`, `user_id`, `achiv_id`) VALUES
(64, 1, 3),
(65, 1, 4),
(66, 1, 9),
(89, 1, 1),
(90, 2, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Achievements`
--
ALTER TABLE `Achievements`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `MatchHistory`
--
ALTER TABLE `MatchHistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matchhistory_ibfk_1` (`winner`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `SaveGame`
--
ALTER TABLE `SaveGame`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `UsersAchivments`
--
ALTER TABLE `UsersAchivments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `usersachivments_ibfk_2` (`achiv_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Achievements`
--
ALTER TABLE `Achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `MatchHistory`
--
ALTER TABLE `MatchHistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT для таблицы `SaveGame`
--
ALTER TABLE `SaveGame`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT для таблицы `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `UsersAchivments`
--
ALTER TABLE `UsersAchivments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `MatchHistory`
--
ALTER TABLE `MatchHistory`
  ADD CONSTRAINT `matchhistory_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `SaveGame`
--
ALTER TABLE `SaveGame`
  ADD CONSTRAINT `savegame_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `UsersAchivments`
--
ALTER TABLE `UsersAchivments`
  ADD CONSTRAINT `usersachivments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usersachivments_ibfk_2` FOREIGN KEY (`achiv_id`) REFERENCES `Achievements` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
