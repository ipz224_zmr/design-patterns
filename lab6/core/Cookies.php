<?
namespace core;

class Cookies
{

    public static function cookiesSetNull($cookies)
    {
        foreach ($cookies as $cookie) {
            setcookie($cookie, '', time() - 3600, '/');
        }
    }
    public static function setcookies($save)
    {
        if ($save!=null) {
            foreach ($save as $key => $value) {
                setcookie($key, $value, time() + 300, "/");
            }
        }
      
    }
    public static function  areCookiesSet($keys)
    {
        foreach ($keys as $key) {
            if (!isset($_COOKIE[$key])) {
                return false;
            }
        }
        return true;
    }

}