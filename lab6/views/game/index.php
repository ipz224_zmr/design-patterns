<?
use models\User;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script defer src="js/Cookies.js"></script>
    <script defer src="js/Observer.js"></script>
    <script defer src="js/table.js"></script>
    <script defer src="js/message.js"></script>
    <script defer src="js/helper.js"></script>
    <script defer src="js/game.js"></script>
    <script defer src="js/oneVsOneTicTacToe.js"></script>
   <script defer src="js/vsBotTicTacToe.js"></script>
    <script defer src="js/ticTacToeFactory.js"></script>
    <script defer src="js/main.js"></script>
</head>
<body>
 
<?php 
$_GET['dificult']  = isset($_GET['dificult']) ? $_GET['dificult'] :'easy';  
$_GET['size']  = isset($_GET['size']) ? $_GET['size'] :'3';   
$_GET['side']  = isset($_GET['side']) ? $_GET['side'] :'X';   
$_GET['type']  = isset($_GET['type']) ? $_GET['type'] :'1vs1';  
$_GET['doubleMove']  = (isset($_GET['doubleMove'])  &&$_GET['size']>3 ) ? $_GET['doubleMove'] :false;  
?>

<div class="row">
<div class="col-6">
    <p>Player <span id="player-turn" class="display-player PlayerX">X</span>'s turn</p>
    <?php if ($_GET['type'] == "vsBot"): ?>
   
        <a class="btn btn-primary m-1 XSide" href="http://lab6/game?type=vsBot&side=X&dificult=<?= $_GET['dificult'] ?>&size=<?= $_GET['size'] ?>">Обрати X</a>
        <a class="btn btn-primary m-1 OSide" href="http://lab6/game?type=vsBot&side=O&dificult=<?= $_GET['dificult'] ?>&size=<?= $_GET['size'] ?>">Обрати O</a>
    <?php if ($_GET['side'] == "O" || $_GET['side'] == "X"): ?>
        <div>

       
        <a class="btn btn-primary m-1" href="http://lab6/game?type=vsBot&side=<?= $_GET['side'] ?>&dificult=easy&size=<?= $_GET['size'] ?>">Легко</a>
        <a class="btn btn-primary m-1" href="http://lab6/game?type=vsBot&side=<?= $_GET['side'] ?>&dificult=middif&size=<?= $_GET['size'] ?>">Середньо</a>
        <a class="btn btn-primary m-1" href="http://lab6/game?type=vsBot&side=<?= $_GET['side'] ?>&dificult=highdif&size=<?= $_GET['size'] ?>">Складно</a>
        </div>
        <?php endif; ?>
<?php endif; ?>
        <a class="btn btn-primary m-1" href="http://lab6/game?type=<?= $_GET['type'] ?>&side=<?= $_GET['side'] ?>&dificult=<?= $_GET['dificult'] ?>&size=3">3на3</a>
        <a class="btn btn-primary m-1" href="http://lab6/game?type=<?= $_GET['type'] ?>&side=<?= $_GET['side'] ?>&dificult=<?= $_GET['dificult'] ?>&size=4">4на4</a>
        <a class="btn btn-primary m-1" href="http://lab6/game?type=<?= $_GET['type'] ?>&side=<?= $_GET['side'] ?>&dificult=<?= $_GET['dificult'] ?>&size=5">5на5</a>   

        <?if( $_GET['size']>3 && $_GET['type']=="1vs1"):?>
            <a class="btn btn-primary m-1" href="http://lab6/game?type=1vs1&size=<?= $_GET['size'] ?>&doubleMove=true">режим 2 кроки</a>
        <?endif;?>
    <div id="gameTable" class="p-3 m-2">

    </div>
    <p class="hide"> <span id="player-won" class="display-player PlayerX">Player Won X</span></p>
    <button class="btn btn-success m-1 btn-backward">Крок назад</button>
</div>

 <div class="col-6">
 <div class="buttons">
 <button id="restartBtn" class="btn btn-success m-1">Перезапуск</button>
<button id="giveUpBtn" class="btn btn-success m-1">Здатися</button>


    <a class="btn btn-success m-1" href="http://lab6/game?type=vsBot&side=X">проти бота</a>
    <a class="btn btn-success m-1" href="http://lab6/game?type=1vs1">1 на 1</a>
    
    
   <?if(User::isUserAuthenticated()&&$_GET['type']=='1vs1'):?>
    <form action="" method="post">
    <button type="submit" class="btn btn-success m-1 SaveGame">Зберегти гру</button>

    </form>
   

    <button class="btn btn-success m-1 ReturnGame" >Повернутися в гру</button>

    <?endif;?>

</div>
 </div>
</div>   
</body>
</html>


