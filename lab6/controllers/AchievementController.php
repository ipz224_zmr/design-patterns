<?php
namespace controllers;

use core\Controller;
use core\Core;
use models\User;
use models\Achievement;
use models\UserAchievement;


class AchievementController extends Controller{
    public function __construct(){
        parent::__construct();
    }
    public function indexAction(){
        $achievements = Achievement::getAchievements();
        $UserAchievements = UserAchievement::getAchievementByUserId();
        
        $array = [];
        foreach ($achievements as $achievement) {
            $array[$achievement['id']] = false; 
            foreach ($UserAchievements as $userAchievement) {
                if ($userAchievement['achiv_id'] === $achievement['id']) {
                    $array[$achievement['id']] = true; 
                    break; 
                }
            }
        }
        return $this->render(null, ['achievements' => $achievements, 'UserAchivHaven' => $array]);
    }
    

    
    public function addAction(){
        if (!User::isAdmin()) {
            return $this->error(403);
        }
        if (Core::get_Instance()->requestMethod == 'POST') {
            if (!empty(trim($_POST['achievementsText']))) {
                Achievement::addAchievement($_POST['achievementsText']);
                return $this->redirect('/achievement/index');
            }
        }
        return $this->render();
    }

    
    public function editAction($params){
        if (empty($params[0])) {
            return $this->redirect('/achievement/index');
        }
        if (!User::isAdmin()) {
            return $this->error(403);
        }
        $id = intval($params[0]);
        $achievement = Achievement::getAchievement($id);
        if (Core::get_Instance()->requestMethod == 'POST') {
            if (!empty(trim($_POST['achievementsText']))) {
            Achievement::editAchievement( $id,$_POST['achievementsText']);
            return $this->redirect('/achievement/index');
            }
        }
        return $this->render(null,['achievement'=>  $achievement]);
    }
    


    public function deleteAction($params){
        if (empty($params[0])) {
            return $this->redirect('/achievement/index');
        }
        if (!User::isAdmin()) {
            return $this->error(403);
        }

        $id = intval($params[0]);
        $yes = isset($params[1]) ? boolval($params[1] == "yes") : false;
        if ($id > 0) {
            $achievement = Achievement::getAchievement($id);
            if ($yes) {
                Achievement::deleteAchievement($id);
                return $this->redirect('/achievement/index');
            }

            return $this->render(null, [
                'achievement' => $achievement
            ]);
        } else {
            return $this->error(403);
        }
    }

}

