﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsClassLibrary
{
    public abstract class Handler : IHandler
    {
        protected abstract string Report { get; }
        protected abstract int HelpLevel { get; }
        protected abstract string HandableProblem { get; }

        private Handler? followingHandler;

        public Handler SetfollowingHandler(Handler followingHandler)
        {
            this.followingHandler = followingHandler;
            return this.followingHandler;
        }

        public virtual void Inquiry(UserInquiry inquiry)
        {
            if (this.followingHandler == null)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Відсутня підтримка для цього питання!");
                Console.ResetColor();
                return;
            }
            else
            {
                this.followingHandler.Inquiry(inquiry);
            }
        }
    }
}
