<?php


namespace controllers;

use models\User;
use core\Cookies;
use core\Controller;
use models\SaveGame;
use models\MatchHistory;
use models\UserAchievement;

 class GameController extends Controller{
    public function __construct(){
        parent::__construct();
    }
    
    public function indexAction(){
        $request_uri_parts = explode('?', $_SERVER['REQUEST_URI']);
        $request_uri_without_query = $request_uri_parts[0];
        
        if ($request_uri_without_query === '/game/index') {
            return   $this->redirect(" https://Lab6/game");
             
           }
        if ( isset($_COOKIE['lostPlayer'])&&User::isUserAuthenticated()) {
             
        UserAchievement::setAchievement();
        }
        if(Cookies::areCookiesSet(['moveHistory', 'winner', 'row', 'col', 'lostPlayer', 'vsBot', 'doubleMove', 'moveHistorySymbol'])) {
            if (User::isUserAuthenticated()) {
                MatchHistory::addMatchHistory([
                    'sequence'     =>  $_COOKIE['moveHistory'],
                    'winner'       =>  $_COOKIE['winner'],
                    'row'          =>  $_COOKIE['row'],
                    'col'          =>  $_COOKIE['col'],
                    'lostPlayer'   =>  $_COOKIE['lostPlayer'],
                    'vsBot'        =>  $_COOKIE['vsBot'],
                    'doubleMove'   =>  $_COOKIE['doubleMove'],
                    'Symbols'      =>  $_COOKIE['moveHistorySymbol']
                ]);
            }
            Cookies::cookiesSetNull(['moveHistory','winner','row','col','lostPlayer','vsBot','doubleMove',"moveHistorySymbol"]);
        }

           if(Cookies::areCookiesSet(['moveHistory', 'playerSide', 'row', 'col', 'vsBot', 'moveHistorySymbol', 'typeGame']) && !isset($_COOKIE['lostPlayer'])){
            
            SaveGame::addSaveGame(['sequence'=> $_COOKIE['moveHistory'],   
            'userSide'=>$_COOKIE['playerSide'],
            'row'          =>  $_COOKIE['row'],
            'vsBot'        =>  $_COOKIE['vsBot'],
            'col'          =>  $_COOKIE['col'],
            'typeGame'   => $_COOKIE['typeGame'],
            "symbols"      => $_COOKIE['moveHistorySymbol']
        ]);
        
        Cookies::cookiesSetNull(['col','moveHistory','moveHistorySymbol','playerSide','row','typeGame','vsBot']); 
       }
       if(User::isUserAuthenticated()){
        $save = SaveGame::getSaveGameByUserId();
        Cookies::setcookies($save);
       }
           
        return $this->render();
    }  



    public function historyAction(){
        if (!User::isUserAuthenticated()) {
                return $this->error(403);
        }
      
        $MatchesHistory =  MatchHistory::getMatchHistories();
        return $this->render(null, ['MatchesHistory' => $MatchesHistory]);
    }  
    
    public function viewAction($params){
        if (!User::isUserAuthenticated()) {
                return $this->error(403);
        }
        if (empty($params[0])) {
            return $this->redirect('/game/history');
        }
        $id = isset($params[0]) ? intval($params[0]) : 0;
        $MatchesHistory =  MatchHistory::getMatchHistorie($id);
     
        return $this->render(null, ['MatchesHistory' => $MatchesHistory]);
    }  
}