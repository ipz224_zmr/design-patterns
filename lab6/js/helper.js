class Helper {
    static disableButtons() {
        const buttons = document.querySelectorAll('.styletable');
        buttons.forEach(button => {
            button.disabled = true;
        });
        const button = document.querySelector('#giveUpBtn');
        button.disabled = true;
    }
}