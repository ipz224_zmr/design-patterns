﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirusLibrary
{

    public interface ICopyableVirus<T> where T : new()
    {
        void PerformCopyTo(T obj);

        public T PerformCopy()
        {
            T t = new();
            PerformCopyTo(t);
            return t;
        }
    }
}
