<?php

namespace models;

class Validate{
    

    public static function validateRegisterForm($RegisterFormData) {
        $errors = [];
    
        if(User::isEmailExists($RegisterFormData['login'])){
            $errors['login'] = 'Користувач з такою почтою вже існує'; 
        }
    
        if (!filter_var($RegisterFormData['login'], FILTER_VALIDATE_EMAIL)) {
            $errors['login'] = 'Помилка при введенні електронної пошти'; 
        }
    
        if (empty(trim($RegisterFormData['password']))) {
            $errors['password'] = 'Пароль пустий';
        }
    
        if (empty(trim($RegisterFormData['password2']))) {
            $errors['password2'] = 'Пароль пустий';
        }
    
        if ($RegisterFormData['password'] != $RegisterFormData['password2']) {
            $errors['password'] = 'Паролі не співпадають';
            $errors['password2'] = 'Паролі не співпадають';
        }
    
        return $errors;
    }
}