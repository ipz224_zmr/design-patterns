﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterClassLibrary
{
   public class FileWriterAdapter : FileWriter ,ILogger
    {
        public FileWriterAdapter(string filePath) :base(filePath) { 
        
        }


        public void Log(string message)
        {
           Console.WriteLine( "File logger "+ message);
        }

        public void Error(string message)
        {
            Console.WriteLine("File logger " + message);
        }

        public void Warn(string message)
        {
            Console.WriteLine("File logger " + message);
        }
    }
}
