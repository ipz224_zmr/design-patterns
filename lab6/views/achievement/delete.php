
<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">Ви точно хочете видалити досягнення: <?=$achievement['achievementsText']?> ?</h4>
  <p>Після видалення досягнення зникне  </p>
  <hr>
  <p class="mb-0">

  <a href="/achievement/delete/<?=$achievement['id']?>/yes"class="btn btn-danger">Видалити досягнення</a>
  <a href="/achievement" class="btn btn-light">Відмінити</a>
  </p>
</div>

