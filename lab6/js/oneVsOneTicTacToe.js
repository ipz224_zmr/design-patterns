class OneVsOneTicTacToe extends TicTacToe {
    constructor(type,row,col) {
        super(type,row,col);
        this.row= row;
        this.col=col;
        this.type = type;
    }
}