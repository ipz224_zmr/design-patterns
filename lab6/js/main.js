import("../js/ticTacToeFactory.js");
const urlParams = new URLSearchParams(window.location.search);
const type = (urlParams.get('type') != null && urlParams.get('type') != "") ? urlParams.get('type') : "1vs1";
const side = urlParams.get('side');
const doubleMove = (urlParams.get('doubleMove') != null && urlParams.get('doubleMove') != "") ? urlParams.get('doubleMove') : false;
let dificult = (urlParams.get('dificult') != null && urlParams.get('dificult') != "") ? urlParams.get('dificult') : "easy";
let size = (urlParams.get('size') != null && urlParams.get('size') != "") ? (urlParams.get('size') >= 3 && urlParams.get('size') <= 5 ? urlParams.get('size') : "3") : "3";

let tableGenerator = new TableGenerator('gameTable');
tableGenerator.generateTable(size, size);

if (type == "1vs1") {
    const game1vs1 = TicTacToeFactory.createGame("1vs1", size, size);
    gameActive(game1vs1, doubleMove,tableGenerator);
} else if (type == "vsBot") {
    const gameVsBot = TicTacToeFactory.createGame("vsBot", size, size, side, dificult);
    gameActive(gameVsBot,tableGenerator);
}






function gameActive(game, doubleMove = false,tableGenerator) {
    document.getElementById('gameTable').addEventListener('click', (event) => {
        if (event.target.classList.contains('styletable')) {
            const position = parseInt(event.target.id.split('-')[1]);
                game.handleMove(position, doubleMove);
        }
    });

    document.querySelector('.btn-backward').addEventListener('click', () => {
        game.undoMove();
    });
    const saveButton = document.querySelector('.SaveGame');
    const returnButton = document.querySelector('.ReturnGame');
    
    if (saveButton && returnButton) {
        saveButton.addEventListener('click', () => {
            game.saveGame();
        });
    
        returnButton.addEventListener('click', () => {
            game.returnGame(tableGenerator);
        });
    } else {
        console.log("Не знайдено одну або обидві кнопки.");
    }
    document.getElementById('restartBtn').addEventListener('click', () => {
        game.restartGame();
    });

    document.getElementById('giveUpBtn').addEventListener('click', () => {
        game.giveUp();
    });

}