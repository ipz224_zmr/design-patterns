<?php


namespace core;


class DB
{
    protected $pdo;

    public function __construct($hostname, $login, $password, $database)
    {
        $this->pdo = new \PDO("mysql: host={$hostname};dbname={$database}", $login, $password);
    }
    public function select($tablename, $fieldsList = "*", $condition = null)
    {
        $queryBuilder = new QueryBuilder();

        $queryBuilder->select($fieldsList)->from($tablename);

        if (!empty($condition)) {
            $queryBuilder->where($condition);
        }

        $query = $queryBuilder->getQuery();
        $params = $queryBuilder->getParams();

        $res = $this->pdo->prepare($query);
        $res->execute($params);

        return $res->fetchAll(\PDO::FETCH_ASSOC);
    }






    public function update($tablename, $newValuesArray, $conditionArray = [])
    {
        $setParts = [];
        $parts = [];
        $paramsArray = [];

        foreach ($newValuesArray as $key => $value) {
            $setParts[] = "{$key} = :set{$key}";
            $paramsArray['set' . $key] = $value;
        }

        $setPartString = implode(' , ', $setParts);

        if (!empty($conditionArray)) {
            foreach ($conditionArray as $key => $value) {
                $parts[] = "{$key} = :{$key}";
                $paramsArray[$key] = $value;
            }

            $wherePart = ' WHERE ' . implode(' AND ', $parts);
        } else {
            $wherePart = '';
        }

        $res = $this->pdo->prepare("UPDATE {$tablename} SET {$setPartString}{$wherePart}");
        return $res->execute($paramsArray);
    }

    public function insert($tablename, $newRowArray)
    {
        $fieldsArray = array_keys($newRowArray);
        $fieldsArrayString = implode(", ", $fieldsArray);
        $paramsArray = [];
        foreach ($newRowArray as $key => $value) {
            $paramsArray[] = ':' . $key;
        }
        $valuesListString = implode(" , ", $paramsArray);
        $res = $this->pdo->prepare("INSERT INTO {$tablename} ($fieldsArrayString) VALUES ($valuesListString)");
        $res->execute($newRowArray);
    }

    public function delete($tablename, $conditionArray)
    {
        $wherePart = [];
        foreach ($conditionArray as $key => $value) {
            $wherePart[] = "{$key} = :{$key}";
        }
        $wherePartString = " WHERE " . implode(' AND ', $wherePart);


        $res = $this->pdo->prepare("DELETE FROM {$tablename} {$wherePartString}");
        $res->execute($conditionArray);
    }


}