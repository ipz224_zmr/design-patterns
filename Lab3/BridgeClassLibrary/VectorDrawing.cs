﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgeClassLibrary
{
    public class VectorDrawing : IRenderer
    {
        public void RenderAs(string text)
        {
            Console.WriteLine($"Drawing {text} as vectors");
        }
    }
}
