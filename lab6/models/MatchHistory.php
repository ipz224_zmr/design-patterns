<?php

namespace models;

class MatchHistory{
    protected static $tableName= 'MatchHistory';

    public static function addMatchHistory($data)
    {
        $user = User::getCurrentAuthenticatedUser();
        \core\Core::get_Instance()->db->insert(
            self::$tableName, [
                'sequence'     => $data['sequence'],
                'winner'       => $data['winner'],
                'row'          => $data['row'],
                'col'          => $data['col'],
                'lostPlayer'   => $data['lostPlayer'],
                'vsBot'        => $data['vsBot'],
                'doubleMove'   => $data['doubleMove'],
                "user_id"      => isset($user['id'])? $user['id']: null,
                "symbols"      => $data['Symbols']
            ]
        );
    }
    public static function getMatchHistories()
    {
        $user= User::getCurrentAuthenticatedUser();
        $rows =  \core\Core::get_Instance()->db->select(
            self::$tableName,"*", [
                'user_id'          =>  $user['id']
            ]
        );
        return !empty($rows) ? $rows : null;
    }
    public static function getMatchHistorie($id)
    {
        $rows =  \core\Core::get_Instance()->db->select(
            self::$tableName,"*", [
                'id'          => $id
            ]
        );
        return !empty($rows) ? $rows : null;
    }
}