<?php
namespace controllers;

use core\Controller;
use core\Core;
use models\User;
use models\Validate;

class UserController extends Controller{
    public function __construct(){
        parent::__construct();
    }
    public function logoutAction(){
      User::logoutUser();
      $this->redirect('/user/login');
  }
    
  
   
    
  public function registerAction() {

    if (User::isUserAuthenticated()) {
        $this->redirect('/');
    }
    if (Core::get_Instance()->requestMethod=='POST') {
        $errors = Validate::validateRegisterForm($_POST);
        if (!empty($errors)) {
            return $this->render(null, ['errors' => $errors]);
        } else {
            User::addUser($_POST['login'], $_POST['password']);
            $this->redirect('/');
        }
    } else { 
        return $this->render();
    }

}

  public function loginAction(){
   if (User::isUserAuthenticated()) {
    $this->redirect('/');
   }
   $error=NULL;
   if(Core::get_Instance()->requestMethod=='POST'){
    $user =  User::getUserByLoginAndPassword($_POST['login'],$_POST['password']);
    if (empty($user)) {
        $error = 'Неправильний логін чи пароль';
    } else{
        User::authenticateUser($user);
        $this->redirect('/');
    }
   }
    return $this->render(null,['error'=>$error]);  
  
 }
 


}

