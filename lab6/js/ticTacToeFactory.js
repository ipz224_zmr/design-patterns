import ("../js/vsBotTicTacToe.js");
import ("../js/oneVsOneTicTacToe.js");

 class TicTacToeFactory {
    static createGame(type, row=3,col=3,side="X", dificult='easy') {
        switch (type) {
            case "1vs1":
                return new OneVsOneTicTacToe("1vs1",row,col);
            case "vsBot":
                return new VsBotTicTacToe("vsBot",row,col,side, dificult);
            default:
                throw new Error("Unsupported game type");
        }
    }
}