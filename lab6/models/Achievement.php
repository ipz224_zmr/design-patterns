<?php

namespace models;

class Achievement{
    protected static $tableName= 'Achievements';

    public static function addAchievement($text)
    {
        \core\Core::get_Instance()->db->insert(
            self::$tableName, [
                'achievementsText'       => $text           
            ]
        );
    }
    public static function deleteAchievement($id)
    {
        \core\Core::get_Instance()->db->delete(
            self::$tableName, [
                'id'       => $id           
            ]
        );
    }
    public static function editAchievement($id,$text)
    {
        \core\Core::get_Instance()->db->update(
            self::$tableName, [
                'achievementsText'       => $text           
            ],['id'=> $id]
        );
    }

    public static function getAchievements()
    {
        $rows =  \core\Core::get_Instance()->db->select(self::$tableName);
        return !empty($rows) ? $rows : null;
    }
    public static function getAchievement($id)
    {
        $rows =  \core\Core::get_Instance()->db->select(
            self::$tableName,"*", [
                'id'          => $id
            ]
        );
        return !empty($rows) ? $rows[0] : null;
    }
}