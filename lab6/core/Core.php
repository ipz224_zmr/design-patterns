<?
namespace core;
use controllers\MainController;
class Core
{
    private static $instance=null;
    public  $app;
    public $db;
    public $pageParams;
    public $requestMethod;
    private function __construct(){
        global $pageParams;
        $this->app = []; 
        $this->pageParams = $pageParams; 
    }
 public static function get_Instance(){
  if (empty(self::$instance)) {
    self::$instance =new self();
  }
  return self::$instance;
 }

 public function Initialize(){
  session_start();
  $this->db = new DB(DATABASE_HOST,DATABASE_LOGIN,
  DATABASE_PASSWORD,DATABASE_NAME);
  $this->requestMethod = $_SERVER['REQUEST_METHOD'];
 }



 public function Run(){
  $route = isset($_GET['route']) ? $_GET['route'] : null;
  $routeParts = $route ? explode("/", $route) : [];
  
  $moduleName = $routeParts ? strtolower(array_shift($routeParts)) : '';
  $actionName = $routeParts ? strtolower(array_shift($routeParts)) : '';

  if (empty($moduleName)) {
    $moduleName = "main";
   }
   if (empty($actionName)) {
    $actionName = "index";
   }
  
   $this->app['moduleName']= $moduleName;
   $this->app['actionName']= $actionName;
   $controllerName = '\\controllers\\'.ucfirst($moduleName).'Controller';
    $controllerActionName = $actionName.'Action';
    $status_code =200;
   if (class_exists($controllerName)) {
    $controller= new $controllerName();
    if (method_exists($controller, $controllerActionName)) {
       $actionResult  =$controller->$controllerActionName($routeParts);
       if ($actionResult instanceof Error) {
        $status_code = $actionResult->code;
       }
       $this->pageParams['content']=    $actionResult;
 
    }
    else{
        $status_code =404;
    } 
   }
   else
   {
    $status_code =404;
   }
    $statusCodeType= intval($status_code/100);
   if ($statusCodeType==4||$statusCodeType==5) {
    $mainController = new MainController();
    $this->pageParams['content']=  $mainController->errorAction($status_code);
   }


 }

 public function Done(){
     $pathToLatout= "themes/light/layon.php";
     $tpl=new Template($pathToLatout);
     $tpl->setParams($this->pageParams);
     $html= $tpl->getHTML();
     echo $html;
 }
}